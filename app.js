var greetings = require('./greet');
var lorem = require('./lorem');

window.onload = function() {
  var h1 = document.getElementsByTagName('h1');
  var article = document.getElementsByTagName('article');
  var img = document.getElementsByTagName('img');
  
  [].forEach.call(h1, function(element){
    element.innerHTML = greetings('Andi');
  }); 
  
  [].forEach.call(article, function(element){
    element.innerHTML = lorem.article();
  }); 
  
  [].forEach.call(img, function(element){
    var ips = require('./ips');
    element.src = 'http://' + ips.get();
  }); 
}
