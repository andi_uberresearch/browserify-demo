var loremIpsum = require('lorem-ipsum');

module.exports = {
  paragraph: function() {
    return loremIpsum({
      count: 1,
      units: 'paragraphs'
    });
  },
  article: function() {
    return loremIpsum({
      count: 7,
      units: 'paragraphs'
    });
  }
}
